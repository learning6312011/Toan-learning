#include<iostream>
using namespace std;
class Frac {
private:
    int Tuso;
    int Mauso;
public:
    Frac(int Tu = 0, int Mau = 1){
        Tuso = Tu;
        Mauso = Mau;
    }
    void NhapPhanSo() {
        cout <<" + Nhap tu so: ";
        cin >> Tuso;
        cout <<"+ Nhap Mau so: ";
        cin >> Mauso;
    }
    void InPhanSo(){
        cout << Tuso << "/" << Mauso << endl;
    }

    bool operator  >= (const Frac& other){
        double val1 = static_cast<double>(Tuso) / Mauso ;
        double val2 = static_cast<double>(other.Mauso) / other.Tuso;
        return val1 >= val2;
    }

};
int main(){
    Frac f[3]; // tao ra 3 dt

    // nhap 3 doi tuong
    for(int i = 0; i < 3; i++){
        cout <<" Nhap thong tin phan so thu: "<< i + 1 << endl;
        f[i].NhapPhanSo() ;
    }
    cout <<" Mang truoc khi duoc sap xep la: "<< endl;
    for(int i = 0; i < 3; i++){
        f[i].InPhanSo () ;
    }

    // tao ra mang 2 chieu de sap xep tang dan
    for(int i = 0; i < 3; i++){
        for(int j = i + 1; j < 3; j++){
            if(f[i] >= f[j]){
                Frac tam = f[i];
                f[i] = f[j];
                f[j] = tam ;
            }
        }
    }


    // In ra pt sau khi da sap xep
    cout <<" Mang sau khi sap xep tang la: "<< endl;
    for(int i = 0; i < 3; i++){
        f[i].InPhanSo() ;
    }
}//
// Created by Administrator on 30/04/2023.
//
