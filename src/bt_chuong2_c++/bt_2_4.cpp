#include<iostream>
#include<string.h>

 using namespace std;

template <typename T>
T Max(T a[], int n){
    T max = a[0];
    for(int i = 0; i < n; i++){
        if(a[i] > max)
            max = a[i];
    }
    return max;
    // tim gt max cua mang
}
int main(){
    // Example for int array, vd mang so nguyen
    int intA[] = {1, 3, 0, 8,2,0, 0, 4};// liet ke cac phan tu
    int intASize = sizeof(intA) / sizeof(int);// dem pt trong mang
    int maxInt = Max(intA, intASize); // so sanh pt mang  A va pt mang  n
    cout <<"gt max trong mang int = \n"<<maxInt<< endl;// in ra gt lon nhat

    // exmple for float array : mang so thuc
    float floatA[] = {1.2, 1.3, 1.4, 1.5};
    float floatASize = sizeof(floatA) / sizeof(float);
    float maxFloat = Max(floatA, floatASize);
    cout <<"gt max trong mang float = \n"<<maxFloat<<endl;

// mang so thuc
char charA[] = {'Q','u', 'y', 'n', 'h'};
char charASize = sizeof(charA) / sizeof(char);
char maxChar = Max(charA, charASize);
cout <<"gt max trong mang char la = "<<maxChar<< endl;
}
