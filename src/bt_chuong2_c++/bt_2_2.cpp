#include<iostream>

using namespace std;

int main() {
    int n,m,p,q;
    cout<<"nhap so hang va so cot mang a: ";
    cin>>n>>m;
    cout<<"nhap so hang va so cot mang b: ";
    cin>>p>>q;

    if(n==p && m==q){
        // cap phat bo nho cho cac mang 2 chieu
        int**a  = new int*[n];
        int** b = new int*[p];
        int** sum = new int*[n];
        int** diff = new int*[n];
        for(int i=0;i<n;i++){
            a[i] = new int[m];
            sum[i] = new int[m];
            diff[i] = new int[m];
            cout<<"nhap hang "<< i+1 <<" mang tran a\n";
            for(int j=0;j<m;j++){
                cout<<"a["<<i<<"]["<<j<<"]=";
                cin>>a[i][j];
            }
        }

        for(int i=0;i<p;i++){
            b[i] = new int[q];
            cout<<"nhap hang"<< i+1 <<"ma tran b\n";
            for(int j=0;j<q;j++){
                cout<<"b["<<i<<"]["<<j<<"]=";
                cin>>b[i][j];
            }
        }

        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                sum[i][j] = a[i][j] + b[i][j];
                diff[i][j] = a[i][j] - b[i][j];
            }
        }

        cout<<"tong cua ma tran a va b: "<<endl;
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                cout<<sum[i][j]<<" ";
            }
            cout<<endl;
        }

        cout<<"hieu cua  tran a va b: "<<endl;
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                cout<<diff[i][j]<<" ";
            }
            cout<<endl;
        }

        for(int i=0;i<n;i++){
            delete[] a[i];
            delete[] sum[i];
            delete[] diff[i];
        }
        for(int i=0;i<p;i++){
            delete[] b[i];
        }
        delete[] a;
        delete[] b;
        delete[] sum;
        delete[] diff;
    } else {
        cout<<"ma tran k cung cap!"<<endl;
    }
    return 0;
}